# r_td_data_management_overview


Objectives : The objective of the study is to describe the duration of hospital stay, the number of treatments and the treatments administered, in relation to the sex of the patient and several age groups (i.e. <18, 19-35, 36-55, 56-75, >75 years old) at the admission in the hospital. 

Inclusion criteria : hospital stays in hospital A (hospital_id = 1) and hospital stay starting after 2021-01-01.

Plan :

- Step 1 : Exploration and design of the data management
- Step 2 : Formatting
- Step 3 : Aggregate and reshape to get the appropriate statistical unit
- Step 4 : Group the different data frames together
- Step 5 : Customize the final dataset
